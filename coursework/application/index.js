import  {Animals, Carnivore, Herbivore} from './Animal/animals';
import Eat from './eat/eat';
import Rect from './decorator/decorator';
import Event from './CustomEvent/CustomEvent';
import Cycle from './naturalAreas/singletonCycle'


const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const food = Object.assign({}, Rect);
let arrBeast = [];
let arrFoods = [];

Cycle();
Event(arrBeast, ctx, Carnivore, Herbivore, arrFoods, food.constructor);
