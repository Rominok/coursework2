class Animals {
    constructor(hunger, life, image, x, y, width, height, ctx, arrFoods, arrBeast){
        this.image = image;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.ctx = ctx;
        this.hunger = hunger;
        this.life = life;
        this.arrFoods = arrFoods;
        this.arrBeast = arrBeast;
    }
    starve(){
      setInterval( () => {
        this.hunger --;
        if(this.hunger < 0){
          this.life--;
        }
      }, 2000);
    }

    draw(){
      this.ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    }
    death(){
      this.ctx.clearRect(this.x, this.y, this.width, this.height);
      if(this.life < 0){

        let img = new Image();
        img.src = 'http://ritualnoe-buro-vrn.ru/images/stories/s-5.png';
        this.image  = img;
        this.image.onload = ()=> {
        this.draw();
        }
        return false;
      }else {
        return true;
      }
    }
    walking(){
      this.arrBeast.push(this);
      setInterval( () => {
        this.starve();

        if(this.death()){

          this.ctx.clearRect(this.x, this.y+20, this.width+5, this.height+5);
          if(this.x < 0){
            this.x += 10;
          }
          else if(this.y<0){
              this.y += 8;
          }
          else if(this.x>400){
            this.x -= 8;
          }
          else if(this.y>200){
              this.y -= 8;
          }
          else{
            let couner = this.randomInteger(10,-10);
            console.log(couner);
            this.x += couner;
            this.y += couner;
          }
        //this.limit();
          this.draw();
          this.eat()
        }
      }, 1000);

    }
    randomInteger(min, max) {
      let rand = min - 0.5 + Math.random() * (max - min + 1)
      rand = Math.round(rand);
      return rand;
    }
    collision(objA, objB) {
      console.log('s');
        if (objA.x+objA.width  > objB.x &&
            objA.x             < objB.x+objB.width &&
            objA.y+objA.height > objB.y &&
            objA.y             < objB.y+objB.height) {
                console.log('true');
                return true;
            }
            else {
              console.log('fals');
                return false;
            }
      }
      eat(){

      };
      // limit(){
      //   this.ctx.strokeRect(this.x+5, this.y+20, this.hunger/20, 1);
      //   //ctx.fillStyle = "green";
      // }
  }//end


 class Carnivore extends Animals{
    eat(){
      this.typeFood = 'carnivore';
      console.log('bred',this.arrBeast);
      this.arrBeast.map( (animal)=>{
        if( this.collision(this, animal) ){
            if(animal.typeFood == 'herbivore'){
              animal.life -=20;
              this.hunger += 40;
            }
        }
      });
    }
   }

   class Herbivore extends Animals {

     eat(){
      this.typeFood = 'herbivore'
       this.arrFoods.map( (food)=>{
         if( this.collision(this, food) ){
             this.hunger += 20;
         }
       });
     }
   }



export {Animals, Carnivore, Herbivore};
