  let Rect ={
        constructor : function(image, x, y, width, height, ctx) {
          this.image = image,
          this.x = x,
          this.y = y,
          this.width = width,
          this.height = height,
          this.ctx = ctx,

          this.draw = () => {
               this.image.onload = ()=> {
                 this.ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
                }
         }
      }
  }

export default Rect;
