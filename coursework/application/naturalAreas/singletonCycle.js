function Cycle(){
  const night = document.querySelector('.nights');
  const season = {
    day: 'day',
    time: 8000,
    dayAndNight: setInterval( () => {
      console.log('time', season.time);
      night.classList.toggle(season.day);
    }, 8000)
  };
  Object.freeze(season);
}

export default Cycle;
