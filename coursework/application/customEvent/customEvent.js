
const Event = (arr, ctx, Carnivore, Herbivore, Eat, newEat) => {

  const startFamine = new CustomEvent('famine');
  const startPlague = new CustomEvent('plague');
  const crateFood = new CustomEvent('addFood');
  const cratePredator = new CustomEvent('addPredator');
  const crateHerbivore = new CustomEvent('addHerbivore');
  const startGame = new CustomEvent('start');

  const buttonFamine = document.querySelector('.famine');
  const buttonPlague = document.querySelector('.plague');
  const buttonAddPredator = document.querySelector('.addPredator');
  const buttonAddHerbivore= document.querySelector('.addHerbivore');
  const buttonAddFood = document.querySelector('.addFood');


  buttonFamine.addEventListener('famine', (e) => {
    arr.forEach( element => {
      console.log('norm',element);
      element.hunger -= 5;
      console.log('hollod',element);
    });
  });

  buttonPlague.addEventListener('plague', (e) => {
      arr.forEach( element => {
      element.life -= 6;
      console.log('chuma');
    });
  });
  //
  buttonAddPredator.addEventListener('addPredator', (e) => {
    console.log('add');
    let imgEat = new Image();
    imgEat.src ='https://d1u5p3l4wpay3k.cloudfront.net/minecraft_ru_gamepedia/thumb/f/fe/PolarBear_Preview.png/280px-PolarBear_Preview.png?version=62cf90366a97513f79a54098b0dfcb3c';

    let preadator = new Carnivore(200,15, imgEat, randomInteger(0,300), randomInteger(0,150), 20, 20, ctx, Eat, arr);
    arr.push(preadator);
    preadator.walking();
  });
  //
  buttonAddHerbivore.addEventListener('addHerbivore', (e) => {
    let imgEat = new Image();
    imgEat.src ='https://vignette.wikia.nocookie.net/minecraft/images/8/8e/150px-Jabit.png/revision/latest?cb=20140913114546&path-prefix=fi';
    let herbivore = new Herbivore(200,10, imgEat, randomInteger(0,300), randomInteger(0,150), 18, 18, ctx, Eat, arr);
    arr.push(herbivore);
    herbivore.walking();
  });
  //
  buttonAddFood.addEventListener('addFood', (e) => {
    let imgEat = new Image();
    imgEat.src ='https://d1u5p3l4wpay3k.cloudfront.net/minecraft_ru_gamepedia/5/5d/%D0%9C%D0%BE%D1%80%D0%BA%D0%BE%D0%B2%D1%8C.png';
    let food = new newEat(imgEat, randomInteger(0,300), randomInteger(0,150), 15, 15, ctx);
    food.draw();
  });

   window.onclick = (e)=>{
    let target = e.target;

    if(target.classList.contains('famine')){
      target.dispatchEvent(startFamine);
    }
    if(target.classList.contains('plague')){
      target.dispatchEvent(startPlague);
    }
    if(target.classList.contains('addFood')){
      target.dispatchEvent(crateFood);
    }
    if(target.classList.contains('addPredator')){
      target.dispatchEvent(cratePredator);
    }
    if(target.classList.contains('addHerbivore')){
      target.dispatchEvent(crateHerbivore);
    }
  }


  window.addEventListener('start', (e)=>{
    for(let i = 2; i > 0; i--){
      buttonAddFood.dispatchEvent(crateFood);
      buttonAddPredator.dispatchEvent(cratePredator);
      buttonAddHerbivore.dispatchEvent(crateHerbivore);
    }
  })
   window.onload = () => window.dispatchEvent(startGame);

  function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
  }
}//end

export default Event;
